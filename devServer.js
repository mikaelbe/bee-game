/* eslint no-var:0 */
var path = require('path');
var express = require('express');
var webpack = require('webpack');
var devMiddleware = require('webpack-dev-middleware');
var config = require('./webpack.config');
var highscoreConfig = require('./src/server/webpack.config');

var app = express();
var compiler = webpack(config);
var highscoreCompiler = webpack(highscoreConfig);

app.use('/highscore', devMiddleware(highscoreCompiler, {
  noInfo: true,
  publicPath: highscoreConfig.output.publicPath,
}));

app.use(devMiddleware(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath,
}));

app.use('/highscore', require('webpack-hot-middleware')(highscoreCompiler));

app.use(require('webpack-hot-middleware')(compiler));

app.use('/assets', express.static('assets'));
app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname, 'src/index.html'));
});

app.listen(3000, 'localhost', function(err) {
  if (err) {
    console.log(err);
    return;
  }

  console.log('Listening at http://localhost:3000');
});
