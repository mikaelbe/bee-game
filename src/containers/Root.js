import React, { Component, PropTypes } from 'react';
import { ReduxRouter } from 'redux-router';
import { Route, IndexRoute } from 'react-router';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';
import App from './App';
import Preload from './Preload';
import Demo from './Demo';
import MainMenu from './MainMenu';
import GamePlay from './GamePlay';
import Review from './Review';
import Debug from './Debug';

const { DevTools, DebugPanel, LogMonitor } =
  __DEV__ ? require('redux-devtools/lib/react'): {};

export default class Root extends Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
  };

  constructor() {
    super();
    this.state = {
      rehydrated: false,
    };
  }

  componentWillMount() {
    persistStore(this.props.store, {
      blacklist: ['gameState', 'router', 'game'],
    }, () => {
      this.setState({ rehydrated: true });
    });
  }

  render() {
    if (!this.state.rehydrated) {
      return <div>Rehydrating ...</div>;
    }

    return (
      <div>
        <Provider store={this.props.store}>
          <ReduxRouter>
            <Route path="/" component={App}>
              <IndexRoute component={Preload} />
              <Route path="/demo" component={Demo} />
              <Route path="/mainmenu" component={MainMenu} />
              <Route path="/gameplay" component={GamePlay} />
              <Route path="/review" component={Review} />
              <Route path="/debug" component={Debug} />
            </Route>
          </ReduxRouter>
        </Provider>
        {() => {
          if (__DEV__) {
            return (
              <DebugPanel top right bottom>
                <DevTools store={this.props.store} monitor={LogMonitor} />
              </DebugPanel>
            );
          }
        }()}
      </div>
    );
  }
}
