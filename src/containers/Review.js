import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { pushState } from 'redux-router';
import { changeGameState, reinitGame } from '../actions';
import Highscore from '../components/Highscore';
import PlayerScore from '../components/PlayerScore';

const style = {
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
  scoreboard: {
    alignSelf: 'flex-start',
    width: '50%',
    height: '100%',
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
  },
  inner: {
    padding: '3%',
  },
  info: {
    alignSelf: 'flex-start',
    width: '50%',
  },
};

@connect((state) => ({
  highscore: state.highscore,
  players: state.players,
  game: state.game,
}))
export default class Review extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    highscore: PropTypes.array,
    players: PropTypes.object,
    game: PropTypes.object,
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(changeGameState('Review'));
  }

  goToMainMenu() {
    const { dispatch } = this.props;
    dispatch(pushState(null, '/mainmenu'));
  }

  restartGame() {
    const { dispatch } = this.props;
    dispatch(reinitGame());
    dispatch(pushState(null, '/gameplay'));
  }

  render() {
    const { highscore, players, game } = this.props;

    return (
      <div style={style.container}>
        <div style={style.scoreboard}>
          <div style={style.inner}>
            <h2>Scoreboard</h2>
            <Highscore highscore={highscore} players={players} />
          </div>
        </div>
        <div style={style.info}>
          <div style={style.inner}>
            {() => {
              return Object.keys(game.players).map((email) => {
                return <PlayerScore player={game.players[email]} players={players} />;
              });
            }()}

            <button onClick={this.goToMainMenu.bind(this)}>
              To Main Menu
            </button>

            <button onClick={this.restartGame.bind(this)}>
              Restart Game
            </button>
          </div>
        </div>
      </div>
    );
  }
}
