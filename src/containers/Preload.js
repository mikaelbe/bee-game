import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { changeGameState } from '../actions';

const style = {
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
  message: {
    fontSize: '3em',
    maxWidth: '50%',
  },
};

@connect(() => ({}))
export default class Preload extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(changeGameState('Preload'));
  }

  render() {
    return (
      <div style={style.container}>
        <div style={style.message}>
          Loading ...
        </div>
      </div>
    );
  }
}
