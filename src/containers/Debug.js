import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { changeGameState } from '../actions';

@connect(() => ({}))
export default class Debug extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(changeGameState('Debug'));
  }

  render() {
    return (
      <div>Debug</div>
    );
  }
}
