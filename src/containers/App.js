import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import GameContainer from './GameContainer';

const style = {
  childContainer: {
    position: 'absolute',
    zIndex: 2,
  },
};

@connect((state) => ({ gameState: state.gameState }))
export default class App extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    gameState: PropTypes.object,
    children: PropTypes.object,
  };

  render() {
    const {width, height} = this.props.gameState;
    const childContainerStyle = Object.assign({}, style.childContainer, {
      width,
      height,
    });

    return (
      <div>
        <div style={childContainerStyle}>
          {this.props.children}
        </div>
        <GameContainer />
      </div>
    );
  }
}
