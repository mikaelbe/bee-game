import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Game from '../game';
import { changeGameSize } from '../actions';

@connect(state => ({ gameState: state.gameState }))
export default class GameContainer extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    gameState: PropTypes.object,
  }

  componentDidMount() {
    this.game = new Game(1920, 1080, Phaser.AUTO, 'game');
  }

  componentWillReceiveProps(nextProps) {
    this.game.state.start(nextProps.gameState.gameState);

    if (this.game.scale) {
      this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
      this.game.scale.refresh();

      this.game.scale.onSizeChange.add(this.onSizeChange, this);
    }
  }

  onSizeChange(ScaleManager, width, height) {
    const { dispatch } = this.props;

    dispatch(changeGameSize({width, height}));
  }

  render() {
    return <div id="game"></div>;
  }
}
