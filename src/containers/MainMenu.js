import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { changeGameState, addPlayer, initGame } from '../actions';
import PlayerRegistration from '../components/PlayerRegistration';
import NextGamePlayer from '../components/NextGamePlayer';
import Highscore from '../components/Highscore';
import { pushState } from 'redux-router';

const MAX_PLAYERS = 2;

const style = {
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
  scoreboard: {
    alignSelf: 'flex-start',
    width: '50%',
    height: '100%',
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
  },
  registration: {
    alignSelf: 'flex-start',
    width: '50%',
  },
  inner: {
    padding: '3%',
  },
  playerRegistration: {
  },
  nextGamePlayers: {
    padding: '20px 0',
  },
  player: {
    backgroundColor: 'rgba(200, 255, 200, 0.4)',
  },
};

@connect((state) => ({players: state.players, game: state.game, highscore: state.highscore }))
export default class MainMenu extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    players: PropTypes.object,
    highscore: PropTypes.array,
  };

  constructor() {
    super();
    this.state = {
      nextGamePlayers: [],
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(changeGameState('MainMenu'));
  }

  onNewPlayer(playerData) {
    const { dispatch } = this.props;
    dispatch(addPlayer(playerData));
  }

  onPlayerRegistered(player) {
    const nextGamePlayers = this.state.nextGamePlayers.concat([player]);
    this.setState({
      nextGamePlayers,
    });
  }

  onRemovePlayer(player) {
    const nextGamePlayers = this.state.nextGamePlayers.filter(pl => pl !== player);
    this.setState({
      nextGamePlayers,
    });
  }

  canStartGame() {
    return this.state.nextGamePlayers.length > 0;
  }

  startGame() {
    const { dispatch } = this.props;
    const { nextGamePlayers } = this.state;

    dispatch(initGame(
      nextGamePlayers.map((player) => player.email)
    ));
    dispatch(pushState({}, '/gameplay'));
  }

  renderPlayerRegistration() {
    const { players } = this.props;
    const { nextGamePlayers } = this.state;

    if (nextGamePlayers.length === MAX_PLAYERS) {
      return <span></span>;
    }

    return (
      <div style={style.playerRegistration}>
        <h2>Add Player {nextGamePlayers.length + 1}</h2>
        <PlayerRegistration
          players={players}
          onNewPlayer={this.onNewPlayer.bind(this)}
          onPlayerRegistered={this.onPlayerRegistered.bind(this)}
          registrationName={`Player ${nextGamePlayers.length + 1}`} />
      </div>
    );
  }

  render() {
    const {nextGamePlayers} = this.state;
    const {highscore, players} = this.props;

    return (
      <div style={style.container}>
        <div style={style.scoreboard}>
          <div style={style.inner}>
            <h2>Scoreboard</h2>
            <Highscore highscore={highscore} players={players} />
          </div>
        </div>
        <div style={style.registration}>
          <div style={style.inner}>

            { this.renderPlayerRegistration() }

            <div style={style.nextGamePlayers}>
              {() => {
                if (nextGamePlayers.length > 0) {
                  return <h2>Up next</h2>;
                }
                return '';
              }()}

              {() => {
                return nextGamePlayers.map((player) => {
                  return (
                    <NextGamePlayer player={player} onRemovePlayer={this.onRemovePlayer.bind(this)} />
                  );
                });
              }()}
            </div>

            <button disabled={!this.canStartGame()} onClick={this.startGame.bind(this)}>
              Start game!
            </button>

          </div>
        </div>
      </div>
    );
  }
}
