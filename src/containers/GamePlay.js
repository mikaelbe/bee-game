import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { changeGameState, addHighscore } from '../actions';
import { pushState } from 'redux-router';

const style = {
  score: {
    paddingLeft: 20,
    paddingTop: 20,
    fontSize: 72,
  },
};

@connect((state) => ({ game: state.game }))
export default class GamePlay extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    game: PropTypes.object,
  };

  constructor() {
    super();
    this.allDead = false;
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(changeGameState('GamePlay'));
  }

  componentWillReceiveProps(nextProps) {
    const playersAlive = Object.keys(nextProps.game.players)
      .map((email) => nextProps.game.players[email])
      .filter((player) => player.alive);


    if (playersAlive.length === 0 && !this.allDead) {
      this.allDead = true;
      setTimeout(() => {
        this.onAllDead();
      }, 3000);
    }
  }

  onAllDead() {
    const { dispatch, game } = this.props;

    Object.keys(game.players)
      .map((email) => game.players[email])
      .forEach((player) => {
        dispatch(addHighscore(player.email, player.finalScore));
      });

    dispatch(pushState({}, '/review'));
  }

  renderScore() {
    const { game } = this.props;
    return (
      <div style={style.score}>
        {game.score}p
      </div>
    );
  }

  render() {
    if (!this.allDead) {
      return this.renderScore();
    }

    return <span></span>;
  }
}
