import Bee from '../objects/bee';
import Gecko from '../objects/gecko';
import ObstacleGroup from '../objects/obstaclegroup';
import Ground from '../objects/ground';
import Background from '../objects/background';
import Midground from '../objects/midground';
import store, { dispatch } from '../store';
import { startGame, incrementScore, killPlayer } from '../actions';

const JUMP_KEYS = [
  Phaser.Keyboard.Z,
  Phaser.Keyboard.M,
];

const PLAYER_OBJECTS = [
  Bee,
  Gecko,
];

export default class GamePlay extends Phaser.State {
  init() {
    this.getState();

    this.unsubscribe = store.subscribe(this.getState.bind(this));
  }

  getState() {
    const { game, players } = store.getState();
    this.appState = game;
    this.globalPlayers = players;
  }

  create() {
    this.game.stage.backgroundColor = '#71c5cf';

    // Use the P2JS physics engine
    this.game.physics.startSystem(Phaser.Physics.P2JS);
    this.game.physics.p2.setImpactEvents(true);
    this.game.physics.p2.gravity.y = 0;
    this.game.physics.p2.restitution = 0.8;

    // Initialize objects
    this.background = new Background(this);
    this.midground = new Midground(this);
    this.ground = new Ground(this);

    // Initialize players
    this.players = Object.keys(this.appState.players)
      .map(key => this.appState.players[key])
      .map((player, idx) =>
        new PLAYER_OBJECTS[idx](
          this.game,
          this.game.world.centerX,
          this.game.world.centerY + 30 * idx,
          this.globalPlayers[player.email],
          JUMP_KEYS[idx]
        )
    );

    // This will hold all the obstacle groups (which will be recycled)
    this.obstacleGroups = this.game.add.group();

    // Initialize gameplay values
    this.isStarted = false;
    this.isCrashed = false;

    // Controls difficulty
    this.distanceStart = 400;   // Distance at start of game
    this.distanceEnd = 50;      // Distance to converge at
    this.difficultyRate = 0.03; // Rate it gets harder at

    this.initCollisions();
    this.initControls();
  }

  initControls() {
    const numPlayers = Object.keys(this.appState.players).length;
    let playersReady = numPlayers;

    JUMP_KEYS.forEach(jumpKey => {
      const key = this.game.input.keyboard.addKey(jumpKey);
      key.onDown.addOnce(() => {
        --playersReady;
        if (playersReady === 0) {
          this.startGame();
        }
      });
    });
  }

  initCollisions() {
    this.collisionGroups = {
      bee: this.game.physics.p2.createCollisionGroup(),
      obstacles: this.game.physics.p2.createCollisionGroup(),
    };

    this.ground.body.setCollisionGroup(this.collisionGroups.obstacles);

    this.players.forEach(player => {
      player.body.setCollisionGroup(this.collisionGroups.bee);
      player.body.collides(this.collisionGroups.obstacles, this.crash, this);
    });

    this.ground.body.collides(this.collisionGroups.bee);

    this.game.physics.p2.updateBoundsCollisionGroup();
  }

  update() {
    this.checkScore();
  }

  startGame() {
    dispatch(startGame());

    this.players.forEach(player => player.start());

    // Enable gravity
    this.game.physics.p2.gravity.y = 2200;

    // Start obstacles
    this.obstacleTimer = this.game.time.events.loop(2000, this.addObstacles, this);
  }

  /**
   * What happens when a bee crashes into something
   */
  crash(crasher, crashee) {
    if (!crasher.sprite.isCrashed) {
      const email = crasher.sprite.playerInfo.email;
      store.dispatch(killPlayer(email));
    }

    crasher.sprite.crash();
    crashee.sprite.crash();

    if (this.players.every(bee => bee.isCrashed)) {
      this.allCrashed();
    }
  }

  /**
   * What happens when all bees have crashed
   */
  allCrashed() {
    if (!this.isCrashed) {
      this.isCrashed = true;
      this.obstacleTimer.pendingDelete = true;
    }
  }

  /**
   * Add an obstacle group and increment score
   */
  addObstacles() {
    const obstY = this.game.rnd.integerInRange(-200, 200);
    const obstDistance = this.obstacleDistance(this.appState.score);
    let obstGroup = this.obstacleGroups.getFirstExists(false);

    if (!obstGroup) {
      obstGroup = new ObstacleGroup(this.game);

      // Set up obstacle groups
      obstGroup.topObstacle.body.setCollisionGroup(
          this.collisionGroups.obstacles);
      obstGroup.topObstacle.body.collides(this.collisionGroups.bee);
      obstGroup.bottomObstacle.body.setCollisionGroup(
          this.collisionGroups.obstacles);
      obstGroup.bottomObstacle.body.collides(this.collisionGroups.bee);

      this.obstacleGroups.add(obstGroup);
    }

    obstGroup.reset(this.game.width + obstGroup.width - 1,
                      obstY + this.game.world.height / 2, obstDistance);
  }

  checkScore() {
    const playersPos = this.players
      .filter(player => !player.isCrashed)
      .map(player => player.x);
    const x = Math.max(...playersPos);

    this.obstacleGroups.forEachExists((obstGroup) => {
      if (!obstGroup.hasScored && obstGroup.topObstacle.x < x) {
        dispatch(incrementScore());
        obstGroup.hasScored = true;
      }
    });
  }

  /**
   * Calculate an obstacle distance
   */
  obstacleDistance(score) {
    return this.distanceStart * Math.exp(-this.difficultyRate * (score + 1)) +
        this.distanceEnd;
  }

  shutdown() {
    this.unsubscribe();
  }
}
