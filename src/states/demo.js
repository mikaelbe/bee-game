import Background from '../objects/background';
import Midground from '../objects/midground';
import Bee from '../objects/bee';
import ObstacleGroup from '../objects/obstaclegroup';
import { pushState } from 'redux-router';
import store from '../store';

export default class Demo extends Phaser.State {
  create() {
    this.game.physics.startSystem(Phaser.Physics.P2JS);
    this.game.physics.p2.setImpactEvents(true);
    this.game.physics.p2.gravity.y = 2200;
    this.game.physics.p2.restitution = 0.8;

    this.background = new Background(this);
    this.midground = new Midground(this);
    this.bee = new Bee(this.game, this.game.world.centerX,
        this.game.world.centerY);
    this.bee.start();

    // This will hold all the obstacle groups (which will be recycled)
    this.obstacleGroups = this.game.add.group();

    // Timer for creating obstacles
    this.obstacleTimer = this.game.time.events.loop(2000, this.addObstacles, this);

    // Input
    this.jumpKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    this.jumpKey.onDown.addOnce(this.startMainMenu, this);
    this.game.input.onDown.addOnce(this.startMainMenu, this);
  }

  startMainMenu() {
    store.dispatch(pushState({}, '/mainmenu'));
  }

  addObstacles() {
    const obstY = this.game.rnd.integerInRange(-200, 200);
    const obstDistance = 400;
    let obstGroup = this.obstacleGroups.getFirstExists(false);

    if (!obstGroup) {
      obstGroup = new ObstacleGroup(this.game);
      obstGroup.topObstacle.body.clearShapes();
      obstGroup.bottomObstacle.body.clearShapes();
      this.obstacleGroups.add(obstGroup);
    }

    obstGroup.reset(this.game.width + obstGroup.width - 1,
        obstY + this.game.world.height / 2, obstDistance);
  }

  update() {
    this.ai();
  }

  ai() {
    const nextObstacleGroup = this.getNearestObstacleGroup();
    let yRef = this.game.world.height / 2;

    if (nextObstacleGroup) {
      const distance = Math.abs(this.bee.x - nextObstacleGroup.topObstacle.position.x);
      yRef += nextObstacleGroup.topObstacle.position.y + distance / 3;
    }

    if (this.bee.y > yRef) {
      this.bee.jump();
    }
  }

  getNextObstacleGroup() {
    const self = this;

    const inFront = this.obstacleGroups.filter((obstGroup) => {
      return obstGroup.topObstacle.x > self.bee.x;
    }, true);

    if (inFront.total) {
      return inFront.list[0];
    }
  }

  getNearestObstacleGroup() {
    let nearest;
    let min = Infinity;

    this.obstacleGroups.forEachExists((obstacleGroup) => {
      const distance = Math.abs(obstacleGroup.topObstacle.position.x - this.bee.x);
      if (distance < min) {
        nearest = obstacleGroup;
        min = distance;
      }
    }, this);

    return nearest;
  }
}
