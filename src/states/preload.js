import { pushState } from 'redux-router';
import store from '../store';

export default class Preload extends Phaser.State {
  preload() {
    this.load.onLoadComplete.addOnce(this.onLoadComplete, this);

    this.game.load.image('background', 'assets/images/Blur-BG.png');
    this.game.load.image('midground', 'assets/images/Blur-BG-Front.png');
    this.game.load.spritesheet('bee', 'assets/images/bie-400x400.png',
        400, 400, 4);
    this.game.load.spritesheet('gecko', 'assets/images/gecko-400x400.png',
        400, 400, 4);
    this.game.load.image('ground', 'assets/images/platform.png');
    this.game.load.image('discoball', 'assets/images/Discoball.png');
    this.game.load.image('chipstack', 'assets/images/chipstack.png');
    this.game.load.image('button', 'assets/images/button.png');
  }

  onLoadComplete() {
    store.dispatch(pushState({}, '/demo'));
  }
}
