import Bee from '../objects/bee';
import Discoball from '../objects/discoball';
import ChipStack from '../objects/chipstack';
import MainMenu from './mainmenu';

export default class Debug extends Phaser.State {
  create() {
    this.game.stage.backgroundColor = '#ffffff';

    this.game.physics.startSystem(Phaser.Physics.P2JS);

    this.bee = new Bee(this.game, 200, 200);
    this.discoball = new Discoball(this.game, 500, 500);
    this.chipStack = new ChipStack(this.game, 1200, 500);

    this.bee.body.debug = true;
    this.discoball.body.debug = true;
    this.chipStack.body.debug = true;
  }

  preload() {
    return MainMenu.prototype.preload();
  }
}
