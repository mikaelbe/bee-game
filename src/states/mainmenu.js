import Background from '../objects/background';
import Midground from '../objects/midground';

export default class MainMenu extends Phaser.State {
  create() {
    this.background = new Background(this);
    this.midground = new Midground(this);
  }
}
