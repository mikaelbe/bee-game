function Score(name, email, score, _id) {
  this.name = name;
  this.email = email;
  this.score = score ? score : 0;
  this._id = _id;
}

Score.prototype.serialize = function() {
  return JSON.stringify(this);
};

module.exports = Score;
