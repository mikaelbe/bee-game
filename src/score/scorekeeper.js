import Score from './score';

function ScoreKeeper(host, port, path) {
  this.host = host;
  this.port = port;
  this.path = path;

  console.warn(`ScoreKeeper is deprecated in its current form`);
}

module.exports = ScoreKeeper;

ScoreKeeper.prototype.getUrl = function() {
  return 'http://' + this.host + ':' + this.port + this.path;
};

ScoreKeeper.prototype.connect = function() {
  if (this.isConnected()) {
    console.warn('Tried to connect, but was already connected.');
    return;
  }
};

ScoreKeeper.prototype.isConnected = function() {
  return !!this.socket;
};

ScoreKeeper.prototype.getHighScores = function(callback) {
  return callback(null, []);
};

ScoreKeeper.prototype.getAccumulatedScores = function(callback) {
  return callback(null, []);
};

/**
 * Save a score object
 */
ScoreKeeper.prototype.saveScore = function(score, callback) {
  return callback(null, {});
};

/**
 * Update server about a score's score
 */
ScoreKeeper.prototype.scoreUpdate = function() {
};
