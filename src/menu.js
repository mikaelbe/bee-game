import { pushState } from 'redux-router';
import remote from 'remote';
import store from './store';
import serverApp from './server';

const Menu = remote.require('menu');
const app = remote.require('app');

function startDebug() {
  store.dispatch(pushState(null, '/debug'));
}

function restartGame() {
  store.dispatch(pushState(null, '/demo'));
}

const template = [
  {
    label: 'Tools',
    submenu: [
      {
        label: 'Debug',
        click: startDebug,
      },
      {
        label: 'Restart Game',
        click: restartGame,
      },
      {
        label: 'Toggle Full Screen',
        accelerator: () => {
          if (process.platform === 'darwin') {
            return 'Ctrl+Command+F';
          }
          return 'F11';
        }(),
        click: (item, focusedWindow) => {
          if (focusedWindow) {
            focusedWindow.setFullScreen(!focusedWindow.isFullScreen());
          }
        },
      },
      {
        type: 'separator',
      },
      {
        label: 'Start server',
        type: 'checkbox',
        checked: serverApp.isRunning,
        click: (item) => {
          serverApp.startServer((err, host, port) => {
            /* eslint no-alert:0 */
            if (err) {
              return window.alert(err);
            }
            item.enabled = false;
            window.alert(`Server running at http://${host}:${port}`);
          });
        },
      },
    ],
  },
];

if (process.platform === 'darwin') {
  const name = remote.require('app').getName();
  template.unshift({
    label: name,
    submenu: [
      {
        label: `About ${name}`,
        role: 'about',
      },
      {
        label: 'Quit',
        accelerator: 'Command+Q',
        click: () => app.quit(),
      },
    ],
  });
}

export default Menu.buildFromTemplate(template);
