import React from 'react';
import { render } from 'react-dom';
import { replaceState } from 'redux-router';
import remote from 'remote';
import Root from './containers/root';
import store from './store';
import menu from './menu';

const rootElement = document.getElementById('react-root');
render(<Root store={store} />, rootElement);

store.dispatch(replaceState({}, '/'));

const body = document.getElementById('body');
if (__DEV__) {
  body.style.width = 'calc(100% - 300px)';
} else {
  body.style.width = '100%';
}

const Menu = remote.require('menu');
Menu.setApplicationMenu(menu);
