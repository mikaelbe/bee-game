import { reduxReactRouter } from 'redux-router';
import { createStore, compose } from 'redux';
import { useBasename } from 'history';
import createHistory from 'history/lib/createHashHistory';
import { autoRehydrate } from 'redux-persist';
import reducer from './reducers';

const { devTools } = __DEV__ ? require('redux-devtools') : {};

const router = reduxReactRouter({
  createHistory: () => useBasename(createHistory)({
    basename: window.location.pathname,
  }),
});

let store;
if (__DEV__) {
  store = compose(router, autoRehydrate(), devTools())(createStore)(reducer);
} else {
  store = compose(router, autoRehydrate())(createStore)(reducer);
}

if (module.hot) {
  module.hot.accept('./reducers', () => {
    const nextRootReducer = require('./reducers');
    store.replaceReducer(nextRootReducer);
  });
}

export default store;
