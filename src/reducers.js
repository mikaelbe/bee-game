import { combineReducers } from 'redux';
import { routerStateReducer } from 'redux-router';
import {
  ADD_PLAYER,
  REMOVE_PLAYER,
  CHANGE_GAME_STATE,
  CHANGE_GAME_SIZE,
  INIT_GAME,
  START_GAME,
  INCREMENT_SCORE,
  KILL_PLAYER,
  ADD_HIGHSCORE,
  REINIT_GAME,
  RECEIVE_ALL_PLAYERS,
  RECEIVE_HIGHSCORE,
} from './actions';

function players(state = {}, action) {
  switch (action.type) {
  case RECEIVE_ALL_PLAYERS:
    return Object.assign({}, action.players);
  case ADD_PLAYER:
    return Object.assign({}, state, { [action.userData.email]: action.userData });
  case REMOVE_PLAYER:
    const _state = Object.assign({}, state);
    delete _state[action.email];
    return _state;
  default:
    return state;
  }
}

function gameState(state = {}, action) {
  switch (action.type) {
  case CHANGE_GAME_STATE:
    /* eslint no-console:0 */
    console.log('Game state ->', action.gameState);
    return Object.assign({}, state, { gameState: action.gameState });
  case CHANGE_GAME_SIZE:
    return Object.assign({}, state, action.dimensions);
  default:
    return state;
  }
}

function _buildPlayers(emails) {
  const gamePlayers = {};
  emails.forEach(email => {
    gamePlayers[email] = { alive: true, email };
  });
  return gamePlayers;
}

function game(state = {}, action) {
  switch (action.type) {
  case INIT_GAME:
    return { players: _buildPlayers(action.emails), score: 0, started: false };
  case START_GAME:
    return Object.assign({}, state, { started: true });
  case INCREMENT_SCORE:
    return Object.assign({}, state, { score: state.score + 1 });
  case KILL_PLAYER:
    const _s = Object.assign({}, state);
    _s.players[action.email] = {
      alive: false,
      finalScore: state.score,
      email: action.email,
    };
    return _s;
  case REINIT_GAME:
    const emails = Object.keys(state.players);
    return { players: _buildPlayers(emails), score: 0, started: false };
  default:
    return state;
  }
}

function highscore(state = [], action) {
  switch (action.type) {
  case RECEIVE_HIGHSCORE:
    return action.highscores;
  case ADD_HIGHSCORE:
    return state
      .concat([{ email: action.email, score: action.score }])
      .sort((a, b) => b.score - a.score);
  default:
    return state;
  }
}

export default combineReducers({
  players,
  gameState,
  game,
  highscore,
  router: routerStateReducer,
});
