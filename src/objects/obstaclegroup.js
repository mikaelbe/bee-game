import Discoball from './discoball';
import ChipStack from './chipstack';

export default class ObstacleGroup extends Phaser.Group {
  constructor(game, parent) {
    super(game, parent);

    this.topObstacle = new Discoball(game);
    this.addChild(this.topObstacle);

    this.bottomObstacle = new ChipStack(this.game);
    this.addChild(this.bottomObstacle);

    this.hasScored = false;
  }

  update() {
    this.checkWorldBounds();
  }

  reset(x, y, distance) {
    this.topObstacle.reset(x, y - this.topObstacle.height / 2 - distance / 2);
    this.bottomObstacle.reset(x, y + this.bottomObstacle.height / 2 + distance / 2);

    this.setAll('body.velocity.x', -400);
    this.exists = true;
    this.hasScored = false;
  }

  checkWorldBounds() {
    this.exists = this.topObstacle.inWorld;
  }
}
