export default class Player extends Phaser.Sprite {
  constructor(game, x, y, sprite, player, jumpKey) {
    super(game, x, y, sprite);

    this.playerInfo = player;

    game.physics.p2.enable(this, false);

    this.isCrashed = false;
    this.isStarted = false;

    this.xRef = x;      // Reference for where the player should be
    this.pAngle = 0.05; // controls speed for going towards reference angle
    this.pPos = 0.05;   // controls speed for going towards xRef (position)

    this.jumpKey = game.input.keyboard.addKey(jumpKey);
    this.jumpKey.onDown.add(this.jump.bind(this));

    if (this.playerInfo) {
      this.text = game.add.text(0, 0, this.playerInfo.displayName, {
        font: '32px Roboto', fill: 'rgba(255, 255, 255, 1.0)',
      });
      this.text.anchor.set(0.5);
    }
  }

  start() {
    this.isStarted = true;
  }

  update() {
    if (this.text) {
      this.text.x = this.body.x - 150;
      this.text.y = this.body.y;
    }

    if (this.isCrashed) {
      return;
    }

    // P-control for angle
    this.body.angularVelocity = this.pAngle * (this.rAngle() - this.body.angle);

    // P-control for x-position
    this.body.velocity.x = this.pPos * (this.xRef - this.body.x);
  }

  rAngle() {
    return 0.05 * this.body.velocity.y + 20;
  }

  jump() {
    if (!this.isCrashed && this.isStarted) {
      this.body.velocity.y = -700;
      this.body.angularForce = -250;
    }
  }

  crash() {
    this.isCrashed = true;
    this.body.force.y = 4000;
    this.body.rotateRight(200);
    this.body.velocity.x = -200;
    this.body.collideWorldBounds = false;
  }
}
