export default class Midground extends Phaser.TileSprite {
  constructor(game) {
    super(game, 0, 0, 1920, 1080, 'midground');

    this.autoScroll(-200, 0);

    game.add.existing(this);
  }
}
