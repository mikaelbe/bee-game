export default class Ground extends Phaser.Sprite {
  constructor(game) {
    super(game, game.world.centerX, game.world.height - 17, 'ground');

    this.scale.setTo(5, 2);

    game.physics.p2.enable(this, false);

    this.body.static = true;

    game.add.existing(this);
  }

  crash() {
    // pass
  }
}
