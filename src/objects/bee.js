import Player from './player';

export default class Bee extends Player {
  constructor(game, x, y, player, jumpKey) {
    super(game, x, y, 'bee', player, jumpKey);

    this.scale.setTo(0.7, 0.7);

    this.body.clearShapes();
    this.body.addCircle(40, -17, 33, 0);
    this.body.addCircle(40, 50, 20, 0);

    game.add.existing(this);

    this.animations.add('fly', [0, 1, 2], 30, true);
    this.animations.add('dead', [3], 1, true);

    this.animations.play('fly');
  }

  crash() {
    super.crash();
    this.animations.play('dead');
  }
}
