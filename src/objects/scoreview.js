import {english} from 'ordinal';
import Score from '../score/score';

const ordinal = english;

export default class ScoreView extends Phaser.Text {
  constructor(game, x, y, score) {
    super(game, x, y, '', { font: '72px Arial', fill: '#fff'});

    this.score = score ? score : new Score();
    this.currPoints = 0;

    game.add.existing(this);
  }

  add(more) {
    this.currPoints += more;

    // Only update the actual score object if currPoints is more -- facilitates
    // re-using the same score object between games.
    if (this.currPoints > this.score.score) {
      this.score.score = this.currPoints;
      this.game.scoreKeeper.scoreUpdate(this.score);
    }
  }

  update() {
    let text = this.currPoints + 'p';

    if (this.currentPlace) {
      text += ' / ' + ordinal(this.currentPlace);
    }

    this.text = text;
  }
}
