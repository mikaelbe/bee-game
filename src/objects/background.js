export default class Background extends Phaser.TileSprite {
  constructor(game) {
    super(game, 0, 0, 1920, 1080, 'background');

    this.autoScroll(-100, 0);

    game.add.existing(this);
  }
}
