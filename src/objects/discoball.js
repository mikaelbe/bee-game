export default class Discoball extends Phaser.Sprite {
  constructor(game, x, y) {
    super(game, x, y, 'discoball');
    game.physics.p2.enable(this);

    this.body.kinematic = true;
    this.body.collideWorldBounds = false;

    this.body.clearShapes();
    this.body.addCircle(135, 0, 155, 0);
    this.body.addRectangle(20, 420, 0, -190, 0);

    game.add.existing(this);
  }

  crash() {
    // pass
  }
}
