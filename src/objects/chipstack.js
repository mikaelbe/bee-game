export default class ChipStack extends Phaser.Sprite {
  constructor(game, x, y) {
    super(game, x, y, 'chipstack');

    game.physics.p2.enable(this);

    this.body.kinematic = true;
    this.body.collideWorldBounds = false;

    this.body.clearShapes();
    this.body.addRectangle(77, 77, -70, -170, 0);
    this.body.addRectangle(61, 61, 25, -163, 0);
    this.body.addRectangle(123, 123, -92, -50, 0);
    this.body.addRectangle(215, 215, 103, -7, 0);
    this.body.addRectangle(182, 182, -123, 122, 0);

    game.add.existing(this);
  }

  crash() {
    // pass
  }
}
