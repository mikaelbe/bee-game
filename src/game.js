import Debug from './states/debug';
import Demo from './states/demo';
import GamePlay from './states/gameplay';
import MainMenu from './states/mainmenu';
import Preload from './states/preload';
import Review from './states/review';

export default class Game extends Phaser.Game {
  constructor(...args) {
    super(...args);
    const {state} = this;

    // Add game states
    state.add('Debug', Debug);
    state.add('Demo', Demo);
    state.add('GamePlay', GamePlay);
    state.add('MainMenu', MainMenu);
    state.add('Preload', Preload);
    state.add('Review', Review);
  }
}
