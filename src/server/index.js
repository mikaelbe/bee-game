import express from 'express';
import store from '../store';
import remote from 'remote';
import path from 'path';

const app = express();

export default app;
export const ctx = {};

app.get('/players.json', (req, res) => {
  const { players } = store.getState();
  res.send(players);
});

app.get('/game.json', (req, res) => {
  const { game } = store.getState();
  res.send(game);
});

app.get('/highscore.json', (req, res) => {
  const { highscore } = store.getState();

  res.send(highscore.slice(0, 50));
});

app.get('/', (req, res, next) => {
  let location = path.join(process.resourcesPath, 'app.asar/build/index-highscore.html');

  if (process.env.BUILD_DEV === '1') {
    location = path.join(remote.process.cwd(), 'src/server/client/index-dev.html');
  }

  res.sendFile(location, (err) => {
    if (err) {
      console.log(err);
      res.status(err.status).end();
    }
    next();
  });
});

app.get('/bundle.js', (req, res, next) => {
  const location = path.join(process.resourcesPath, 'app.asar/build/highscore-bundle.js');
  res.sendFile(location, (err) => {
    if (err) {
      console.log(err);
      res.status(err.status).end();
    }
    next();
  });
});

app.get('/background.png', (req, res, next) => {
  let location = path.join(process.resourcesPath, 'app.asar/assets/images/Blur-BG.png');

  if (process.env.BUILD_DEV === '1') {
    location = path.join(remote.process.cwd(), 'assets/images/Blur-BG.png');
  }

  res.sendFile(location, (err) => {
    if (err) {
      console.log(err);
      res.status(err.status).end();
    }
    next();
  });
});

app.isRunning = false;

app.startServer = (cb) => {
  ctx.server = app.listen(1337, (err) => {
    /* eslint no-console:0 */
    if (err) {
      return cb(err);
    }

    const host = ctx.server.address().address;
    const port = ctx.server.address().port;

    console.log(`Server running at http://${host}:${port}`);

    app.isRunning = true;

    cb(null, host, port);
  });
};
