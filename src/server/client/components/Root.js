import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Highscore from '../../../components/Highscore';

@connect((state) => ({ players: state.players, highscore: state.highscore }))
export default class Root extends Component {
  static propTypes = {
    players: PropTypes.object,
    highscore: PropTypes.array,
  };

  render() {
    const styles = require('./Root.scss');
    const { players, highscore } = this.props;
    return (
      <div className={styles.root}>
        <h1>Highscore</h1>
        <Highscore players={players} highscore={highscore} />
      </div>
    );
  }
}
