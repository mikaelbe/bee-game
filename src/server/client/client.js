import React from 'react';
import { render } from 'react-dom';
import Root from './components/Root';
import { Provider } from 'react-redux';

import store from './store';
import { receiveAllPlayers, receiveHighscore } from '../../actions';

const UPDATE_INTERVAL = 1000;

function updateFromServer() {
  fetch('/players.json')
    .then(response => response.json())
    .then(players => {
      store.dispatch(receiveAllPlayers(players));
    });

  fetch('/highscore.json')
    .then(response => response.json())
    .then(highscore => {
      store.dispatch(receiveHighscore(highscore));
    });
}

updateFromServer();
setInterval(updateFromServer, UPDATE_INTERVAL);

const rootElement = document.getElementById('react-root');
render(
  <Provider store={store}>
    <Root />
  </Provider>,
  rootElement
);
