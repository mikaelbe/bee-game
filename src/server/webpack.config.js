/* eslint no-var:0 */
var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: [
    './src/server/client/client.js',
  ],
  output: {
    path: process.env.BUILD_DEV === '1' ? path.join(__dirname, 'build') : 'build',
    filename: 'highscore-bundle.js',
    publicPath: process.env.BUILD_DEV === '1' ? 'http://localhost:3000/' : '',
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        include: path.join(__dirname, '..'),
        loaders: ['babel?stage=0'],
      },
      { test: /\.scss$/, loader: 'style!css?module&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss' },
    ],
  },
};

if (process.env.BUILD_DEV === '1') {
  module.exports.devtool = 'eval';
  module.exports.debug = true;

  module.exports.plugins = [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
  ];
}
