/*
 * Action Types
 */
export const ADD_PLAYER = 'ADD_PLAYER';
export const REMOVE_PLAYER = 'REMOVE_PLAYER';
export const CHANGE_GAME_STATE = 'CHANGE_GAME_STATE';
export const CHANGE_GAME_SIZE = 'CHANGE_GAME_SIZE';
export const INIT_GAME = 'INIT_GAME';
export const START_GAME = 'START_GAME';
export const INCREMENT_SCORE = 'INCREMENT_SCORE';
export const KILL_PLAYER = 'KILL_PLAYER';
export const ADD_HIGHSCORE = 'ADD_HIGHSCORE';
export const REINIT_GAME = 'REINIT_GAME';
export const RECEIVE_ALL_PLAYERS = 'RECEIVE_ALL_PLAYERS';
export const RECEIVE_HIGHSCORE = 'RECEIVE_HIGHSCORE';

/*
 * Action Creators
 */
export function receiveAllPlayers(players) {
  return { type: RECEIVE_ALL_PLAYERS, players};
}

export function addPlayer(userData) {
  return { type: ADD_PLAYER, userData };
}

export function removePlayer(email) {
  return { type: REMOVE_PLAYER, email };
}

export function changeGameState(gameState) {
  if (typeof newState === 'string') {
    throw new Error(`Expected a string gameState, got ${gameState}`);
  }

  return { type: CHANGE_GAME_STATE, gameState };
}

export function changeGameSize(dimensions) {
  if (!Number.isInteger(dimensions.width) || !Number.isInteger(dimensions.height)) {
    throw new Error(`Expected width/height dimensions, got ${dimensions}`);
  }

  return { type: CHANGE_GAME_SIZE, dimensions };
}

export function initGame(emails) {
  if (!Array.isArray(emails)) {
    throw new Error(`Expected an array of email addresses, got ${emails}`);
  }

  return { type: INIT_GAME, emails };
}

export function startGame() {
  return { type: START_GAME };
}

export function incrementScore() {
  return { type: INCREMENT_SCORE };
}

export function killPlayer(email) {
  return { type: KILL_PLAYER, email };
}

export function receiveHighscore(highscores) {
  return { type: RECEIVE_HIGHSCORE, highscores };
}

export function addHighscore(email, score) {
  return { type: ADD_HIGHSCORE, email, score };
}

export function reinitGame() {
  return { type: REINIT_GAME };
}
