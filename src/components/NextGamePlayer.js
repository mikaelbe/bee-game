import React from 'react';

import styles from './NextGamePlayer.scss';

const NextGamePlayer = props => {
  const { player, onRemovePlayer } = props;
  return (
    <div className={styles.nextGamePlayer}>
      {player.displayName}
      <button className={styles.removePlayer} onClick={() => onRemovePlayer(player)}>remove</button>
    </div>
  );
};

export default NextGamePlayer;
