import React, { Component, PropTypes } from 'react';

import styles from './Highscore.scss';

export default class Highscore extends Component {
  static propTypes = {
    highscore: PropTypes.array.isRequired,
    players: PropTypes.object.isRequired,
  };

  render() {
    const { highscore, players } = this.props;

    return (
      <table className={styles.highscoreTable}>
        <tbody>
          {() => {
            let position = 0;
            let prevScore = Infinity;
            return highscore.map((score, i) => {
              let updated = false;

              if (score.score < prevScore) {
                position++;
                prevScore = score.score;
                updated = true;
              }

              return (
                <tr key={i} className={updated ? styles.updated : ''}>
                  <td>{updated ? position : ''}</td>
                  <td>{players[score.email].displayName}</td>
                  <td className={styles.scoreColumn}>{score.score} points</td>
                </tr>
              );
            });
          }()}
        </tbody>
      </table>
    );
  }
}
