import React, { Component, PropTypes } from 'react';

export default class PlayerScore extends Component {
  static propTypes = {
    player: PropTypes.object.isRequired,
    players: PropTypes.object.isRequired,
  };

  render() {
    const { player, players } = this.props;

    return (
      <div>
        <h2>{players[player.email].displayName} scored {player.finalScore} points</h2>
      </div>
    );
  }
}
