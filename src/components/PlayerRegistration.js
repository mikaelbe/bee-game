import React, { Component, PropTypes } from 'react';

import styles from './PlayerRegistration.scss';

export default class PlayerRegistration extends Component {
  static propTypes = {
    players: PropTypes.object.isRequired,
    onNewPlayer: PropTypes.func,
    onPlayerRegistered: PropTypes.func,
    registrationName: PropTypes.string.isRequired,
  };

  constructor() {
    super();
    this.state = {};
  }

  onRegistrationComplete() {
    const { email, displayName, existingPlayer } = this.state;
    const { onNewPlayer, onPlayerRegistered } = this.props;

    if (!existingPlayer && onNewPlayer) {
      onNewPlayer({ email, displayName });
    }

    onPlayerRegistered({ email, displayName });

    this.refs.email.value = '';
    this.refs.displayName.value = '';
    this.setState({
      email: undefined,
      displayName: undefined,
      existingPlayer: false,
    });
  }

  onEmailChange(event) {
    const { players } = this.props;
    const email = event.target.value.trim();

    if (players[email]) {
      this.setState({
        email,
        displayName: players[email].displayName,
        existingPlayer: true,
      });
    } else {
      this.setState({ email, existingPlayer: false });
    }
  }

  onDisplayNameChange(event) {
    const displayName = event.target.value.trim();
    this.setState({displayName});
  }

  onSubmitForm(event) {
    event.preventDefault();
    this.onRegistrationComplete();
  }

  isRegistrationComplete() {
    return this.state.email && this.state.displayName;
  }

  renderEmailRow() {
    return (
      <div className={styles.formRow}>
        <label>Email</label>
        <input
          type="email"
          ref="email"
          placeholder="E.g. you@company.com"
          onChange={this.onEmailChange.bind(this)}
          required
          />
      </div>
    );
  }

  renderDisplayNameRow() {
    const { existingPlayer } = this.state;

    return (
      <div className={styles.formRow}>
        <label>Nickname</label>
        {() => {
          if (existingPlayer) {
            return <input type="text" disabled ref="displayName" value={this.state.displayName} />;
          }
          return <input type="text" ref="displayName" onChange={this.onDisplayNameChange.bind(this)} required />;
        }()}
      </div>
    );
  }

  render() {
    return (
      <form onSubmit={this.onSubmitForm.bind(this)}>
        {this.renderEmailRow()}
        {this.renderDisplayNameRow()}
        <button type="submit" disabled={!this.isRegistrationComplete()}>
          Add player
        </button>
      </form>
    );
  }
}
