#!/bin/bash

# See photonstorm/phaser#1974
# https://github.com/photonstorm/phaser/issues/1974#issuecomment-134222165

# Clone the Phaser repo
git clone https://github.com/photonstorm/phaser.git .phaser-tmp && cd .phaser-tmp;
git checkout v2.4.4

# Run NPM install
npm install;

# Generate custom Phaser build
grunt custom --exclude creature,ninja --split true;

# Copy generated files to lib folder
mkdir -p ../lib
mkdir dist
cp dist/p2.js ../lib/p2.js;
cp dist/phaser.js ../lib/phaser.js;
cp dist/pixi.js ../lib/pixi.js;

# Remove traces
cd ..;
rm -rf .phaser-tmp/
