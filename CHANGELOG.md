<a name="2.1.0"></a>
## 2.1.0 (2016-01-21)

#### Features
* **game:** Go back to using old graphics

<a name="2.0.0"></a>
## 2.0.0 (2015-12-31)


#### Bug Fixes

* **objects:** Fix `discoball` hitbox ((8669717a))
* **player:** Set color of names to opaque ((87975fb8))


#### Features

* **game:** Color adjustments ((0a9c3fb9))


<a name="2.0.0-beta.3"></a>
### 2.0.0-beta.3 (2015-12-21)


#### Bug Fixes

* **game:** Use the new background ((b0588623))


<a name="2.0.0-beta.2"></a>
### 2.0.0-beta.2 (2015-12-21)


#### Features

* **game:** Updated graphics ((eaa2a43f))


<a name="2.0.0-beta.1"></a>
### 2.0.0-beta.1 (2015-12-03)


#### Bug Fixes

* **gameplay:** Start when both players have pressed their button ((12f3a95b))


#### Features

* **Game:** Replace background with new background for 2016 ((6a4c0740))
* **demo:** Start bee in demo mode ((2738c25c))
* **server:**
  * Add background image to highscore list ((98304e65))
  * Update at regular interval (5s) ((781b0fc9))
  * Reuse highscore component ((5aea9e8d))


<a name="2.0.0-alpha.2"></a>
### 2.0.0-alpha.2 (2015-11-18)


#### Bug Fixes

* **Demo:** Fix typo in demo screen ((cfb76f62))
* **PlayerRegistration:** Fix to be able to add several players ((14ab6d61))
* **game:** Fix problem where scroll bars would appear on Windows ((9e7e0fb5))
* **reducers:** Handle case where theres n > 1 players ((aa4a358e))


#### Features

* **Highscore:** Beautify highscore ((f9d91df7))
* **MainMenu:** Nicer rendering of registered players ((e5bb23da))
* **PlayerRegistration:** Display both Email and Nickname fields simultaneously ((82ab688c))
* **app:**
  * Add a `gecko` object and use it in the game ((b2f3a69e))
  * Support for different `player` objects ((fc075229))
  * Quit (Cmd+Q) on Darwin ((bd5eab6e))
  * Add `Toggle Full Screen` to `tools` menu ((fd6f3c07))
* **game:**
  * Display players' names in gameplay ((3d62654f))
  * Two player working ((5282fca8))
* **highscore:**
  * Show a list of players ((51e5708c))
* **pkg:**
  * Add hot reloading of React components ((0b563408))
  * Add hot module replacement with web server ((eaa6fea6))
  * Start without dev-tools with `npm run start-prod` ((4aafbaa7))
* **server:**
  * Serve data from store as JSON ((f85b2627))
  * Start a server from the menu ((5e1ea723))


<a name="2.0.0-alpha.1"></a>
### 2.0.0-alpha.1 (2015-10-28)

Rewrite as an Electron app using ES2016 via Babel. Also using Redux as a Flux
pattern and React for rendering non-game views.


<a name="1.0.0"></a>
## 1.0.0 (2015-10-19)

Initial version of the game, which was used at Embedded World 2015. The product
is a JavaScript (ECMAScript 5) library and a HTML file, built with Grunt.

Key features:

  - Base game
  - Users can enter their name and email
  - Connected with a Scoreboard instance for keeping track of high scores
  - Typically run with a Docker container for the Scoreboard
