/* eslint no-var:0 */
var webpack = require('webpack');
var path = require('path');
var webpackTargetElectronRenderer = require('webpack-target-electron-renderer');

var config = {
  devtool: 'eval',
  debug: true,
  entry: [
    './src/index.js',
  ],
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'bundle.js',
    publicPath: process.env.BUILD_DEV === '1' ? 'http://localhost:3000/' : '',
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        include: path.join(__dirname, 'src'),
        loaders: ['babel'],
      },
      {
        test: /\.json$/,
        loaders: ['json'],
      },
      { test: /pixi.js/, loader: 'script' },
      { test: /phaser.js/, loader: 'script' },
      { test: /p2.js/, loader: 'script' },
      { test: /\.scss$/, loader: 'style!css?module&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss' },
      { test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/, loader: 'file-loader' },
      { test: /\.(jpe?g|png|gif|svg)$/i, loaders: [
        'file?hash=sha512&digest=hex&name=[hash].[ext]',
        'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false',
      ]},
    ],
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.DefinePlugin({
      __DEV__: JSON.stringify(JSON.parse(process.env.BUILD_DEV || 'true')),
    }),
  ],
};

if (process.env.BUILD_DEV === '1') {
  config.entry.unshift('webpack-hot-middleware/client?path=http://localhost:3000/__webpack_hmr');
  config.plugins = config.plugins.concat([
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
  ]);
}

config.target = webpackTargetElectronRenderer(config);

module.exports = config;
