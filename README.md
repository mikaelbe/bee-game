# Bee Game

Help a rather flappy bee navigate its way between obstacles.

This is a simple game implemented with [Phaser](http://phaser.io), a JavaScript game engine. The gameplay is very simple, one controller to make the bee "jump", i.e. gain some height.

## Issues

- [ ] Needs to build without `--prune`, see [caprine#19](https://github.com/sindresorhus/caprine/issues/19)


## Usage

See `src/index.html`.

### Using bundled version

```html
<div id="game"></div>
<script src="beegame.bundle.min.js"></script>
<script>
  'use strict';
  /* global BeeGame */
  (function() {
    BeeGame.create('#game');
  })();
</script>
```

### Using Phaser directly

```html
<script src="phaser.js"></script>
<script src="beegame.min.js"></script>
<script>
  'use strict';
  /* global BeeGame */
  (function() {
    BeeGame.create('#game');
  })();
</script>
```

## Development

Requirements:
 - NodeJS

Run `npm install` to install dependencies, then use `grunt serve` to pull up a development server.

Use `grunt serve:dist` to run a server with compressed distributable version, or `grunt:build` to just build it.

## Testing

No testing yet, contribute if you'd like!

## License

MIT &copy; Silicon Laboratories
